package ab1.impl.Dippold;

import ab1.RegEx;

public class RegExImpl implements RegEx {

    @Override
    public String getRegex1() {
        String regex = ".*a+b+.*";
	return regex;
    }

    @Override
    public String getRegex2() {
        String regex = ".*(?:abc|def).*";
	return regex;
    }

    @Override
    public String getRegex3() {
        String regex = ".*(?:abc).*(?:abc).*(?:abc).*";
        return regex;
    }

    @Override
    public String getRegexURL() {
        String protocol = "(?:https?|ftp):\\/\\/";
        String domain = "(?:[a-zA-Z0-9-_]+\\.)*[a-zA-Z0-9-_]+\\.[a-zA-Z]+";
        String port = "(?::[0-9]+)?";
        String directory = "(?:\\/[a-zA-Z0-9-_.]+)*\\/?";
        return '^' + protocol + domain + port + directory + '$';
    }

    @Override
    public String multiMatch1() {
        String regex = "[A-ZÄÖÜa-zäöü]*([^dr])oo[A-ZÄÖÜa-zäöü]*";
        return regex;
    }

    @Override
    public String multiMatch2() {
        String regex = "[A-ZÄÖÜa-zäöü]+ick$";
        return regex;
    }

    @Override
    public String multiMatch3() {
        String regex = "([a-z]{0,3}fu)$";
        return regex;
    }

    @Override
    public String multiMatch4() {
        String regex = "(\\\\(w[+*])?($|\\?{3}))|" +					// \w+, \w*, \???
                "((a\\{[139](,8)?\\}\\|?)+)|" +							// a{1}|a{3}|a{9}, a{1,8}
                "(^-((\\+-)*\\+)?$)|" +									// -, -+-+-+-+
                "(\\(\\\\((w\\+\\?)|(ufacdf))\\)((\\\\7)|(\\*\\?)))|" +	// (\w+?)\7, (\ufacdf)*?
                "(\\[([A0akv]-[Z9foz])+\\][+*])|" +						// [A-Z0-9]+, [a-fk-ov-z]*
                "(\\(\\?:\\[aeiou\\]\\+\\)\\\\1\\+)|" +					// (?:[aeiou]+)\1+
                "(\\[a\\\\-z\\\\-9\\])|" +								// [a\-z\-9]
                "(\\(\\\\((001)|(2))\\)\\\\1)";
        return regex;
    }

}
